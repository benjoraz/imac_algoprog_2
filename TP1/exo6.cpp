#include <iostream>
using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    // your code
};

struct DynaTableau{
    int* donnees;
    // your code
    int size;
    int capacity;
};


void initialise(Liste* liste)
{
    liste->premier = nullptr;
}

bool est_vide(const Liste* liste)
{
    if (liste->premier == nullptr)
        {
            return true;
        }
    return false;
}

void ajoute(Liste* liste, int valeur)
{
    Noeud* nouveauNoeud = new Noeud;
    nouveauNoeud->donnee = valeur;
    nouveauNoeud->suivant = nullptr;

    if (liste->premier == nullptr)
    {
        liste->premier = nouveauNoeud;
    }
    else
    {
        Noeud* dernier = liste->premier;
        while (dernier->suivant != nullptr)
        {
            dernier = dernier->suivant;
        }
        dernier->suivant = nouveauNoeud;
    }

}

void affiche(const Liste* liste)
{
    if (liste->premier != nullptr)
    {
        Noeud* noeud = liste->premier;
        while (noeud != nullptr)
        {
            cout << noeud->donnee << endl;
            noeud = noeud->suivant;
        }
    }

}

int recupere(const Liste* liste, int n)
{
    Noeud* noeud = liste->premier;
    int i = 0;
    while (i < n)
    {
        noeud = noeud->suivant;
        i++;
    }
    return noeud->donnee;

}

int cherche(const Liste* liste, int valeur)
{
    Noeud* noeud = liste->premier;
    int i = 0;
    while (noeud->donnee != valeur)
    {
        noeud = noeud->suivant;
        i++;
    }
    return i + 1;
}

void stocke(Liste* liste, int n, int valeur)
{
    Noeud* noeud = liste->premier;
    int i = 0;
    while (i < n)
    {
        noeud = noeud->suivant;
        i++;
    }
    noeud->donnee = valeur;
}

void ajoute(DynaTableau* tableau, int valeur)
{
//    if (tableau->size + 1 > tableau->capacity)
//    {
//        tableau->capacity += 5;
//        int* tab = new int[tableau->capacity];
//        for (int i = 0; i < tableau->size; i++)
//        {
//            tab[i] = tableau->donnees[i];
//        }
//        delete[] tableau->donnees;
//        tableau->donnees = tab;
//    }
//    tableau->donnees[tableau->size] = valeur;
//    tableau->size += 1;

    int* donnees = tableau->donnees;
    if(tableau->size < tableau->capacity)
    {
        donnees[tableau->size+1]=valeur;
        tableau->size++;
    }
    else
    {
        tableau->capacity += 2;
        int* tab = new int[tableau->capacity];
        if(tab == nullptr)
        {
            exit(1);
        }
        for(int i=0; i<tableau->size; i++)
        {
            tab[i] = donnees[i];
        }
        delete[] tableau->donnees;
        tab[tableau->size] = valeur;
        tableau->size += 1;
        donnees = tab;
    }
}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau->donnees = new int[capacite];
    tableau->size = 0;
    tableau->capacity = capacite;

}

bool est_vide(const DynaTableau* liste)
{
    if (liste->size == 0)
    {
        return true;
    }
    else
    {
        return false;
    }

}

void affiche(const DynaTableau* tableau)
{
    for (int i = 0; i < tableau->size; i++)
    {
        cout << tableau->donnees[i] << endl;
    }

}

int recupere(const DynaTableau* tableau, int n)
{
    if(n >= tableau->size)
    {
        exit(1);
    }
    return tableau->donnees[n];
}

int cherche(const DynaTableau* tableau, int valeur)
{
    for (int i = 0; i < tableau->size; i++)
    {
        if (tableau->donnees[i] == valeur)
        {
            return i;
        }
    }
    return -1;

}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    if(n >= tableau->size)
    {
        exit(1);
    }
    tableau->donnees[n] = valeur;
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    Noeud* nouveauNoeud = new Noeud;
    nouveauNoeud->donnee = valeur;
    nouveauNoeud->suivant = nullptr;

    if (liste->premier == nullptr)
    {
        liste->premier = nouveauNoeud;
    }
    else
    {
        Noeud* dernier = liste->premier;
        while (dernier->suivant != nullptr)
        {
            dernier = dernier->suivant;
        }
        dernier->suivant = nouveauNoeud;
    }
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    int value = liste->premier->donnee;
    liste->premier = liste->premier->suivant;
    return value;

}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{
    Noeud* nouveauNoeud = new Noeud;
    nouveauNoeud->donnee = valeur;
    nouveauNoeud->suivant = nullptr;

    if (liste->premier == nullptr)
    {
        liste->premier = nouveauNoeud;
    }
    else
    {
        Noeud* dernier = liste->premier;
        while (dernier->suivant != nullptr)
        {
            dernier = dernier->suivant;
        }
        dernier->suivant = nouveauNoeud;
    }
}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste)
{
    if (liste->premier != nullptr)
    {
        Noeud* noeud = liste->premier;
        Noeud* noeud1;

        if (noeud->suivant != nullptr)
        {
            noeud1 = noeud->suivant;
            while (noeud1->suivant != nullptr)
            {
                noeud = noeud->suivant;
                noeud1 = noeud->suivant;
            }
            noeud->suivant = nullptr;
            return noeud1->donnee;
        }
        else
        {
            liste->premier = nullptr;
            return noeud->donnee;
        }
    }
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        cout << "Oups y a une anguille dans ma liste" << endl;
    }

    if (!est_vide(&tableau))
    {
        cout << "Oups y a une anguille dans mon tableau" << endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        cout << "Oups y a une anguille dans ma liste" << endl;
    }

    if (est_vide(&tableau))
    {
        cout << "Oups y a une anguille dans mon tableau" << endl;
    }

    cout << "Elements initiaux:" << endl;
    affiche(&liste);
    affiche(&tableau);
    cout << endl;

    cout << "5e valeur de la liste " << recupere(&liste, 4) << endl;
    cout << "5e valeur du tableau " << recupere(&tableau, 4) << endl;

    cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << endl;
    cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    cout << "Elements après stockage de 7:" << endl;
    affiche(&liste);
    affiche(&tableau);
    cout << endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        cout << retire_file(&file) << endl;
        compteur--;
    }

    if (compteur == 0)
    {
        cout << "Ah y a un soucis là..." << endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        cout << retire_pile(&pile) << endl;
        compteur--;
    }

    if (compteur == 0)
    {
        cout << "Ah y a un soucis là..." << endl;
    }

    return 0;
}
