#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    return nodeIndex*2 + 1;
}

int Heap::rightChild(int nodeIndex)
{
    return nodeIndex*2 + 2;
}

void Heap::insertHeapNode(int heapSize, int value)
{
    // use (*this)[i] or this->get(i) to get a value at index i
    int i = heapSize;
    (*this)[i] = value;
    while(i >= 0 && (*this)[i]>(*this)[(i-1)/2])
    {
        swap(i, (i-1)/2);
        i=(i-1)/2;
    }
}

void Heap::heapify(int heapSize, int nodeIndex)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i_max = nodeIndex;
    int i_right = this->rightChild(nodeIndex);
    int i_left = this->leftChild(nodeIndex);
    if(i_left < heapSize && this->get(i_left)>this->get(i_max))
    {
        i_max = i_left;
    }
    if(i_right < heapSize && this->get(i_right)>this->get(i_max))
    {
        i_max = i_right;
    }
    if(i_max != nodeIndex)
    {
        swap(i_max, nodeIndex);
        heapify(heapSize, i_max);
    }
}

void Heap::buildHeap(Array& numbers)
{
    for(int i = 0; i < numbers.size(); i++)
    {
        this->insertHeapNode(this->size(), numbers[i]);
    }
}

void Heap::heapSort()
{
    for(int i= this->size()-1; i>=0; i--)
    {
        this->swap(0,i);
        this->heapify(i,0);
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
