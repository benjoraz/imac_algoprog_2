#include "tp6.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;

void Graph::buildFromAdjenciesMatrix(int **adjacencies, int nodeCount)
{
	/**
	  * Make a graph from a matrix
	  * first create all nodes, add it to the graph then connect them
	  * this->appendNewNode
	  * this->nodes[i]->appendNewEdge
	  */
    for(int i=0; i<nodeCount; i++)
    {
        GraphNode* newNode = new GraphNode(i);
        this->appendNewNode(newNode);
    }
    for(int i=0; i<nodeCount; i++)
    {
        for(int j=0; j<nodeCount; j++)
        {
            int newValue = adjacencies[i][j];
            if(newValue != 0)
            {
                this->nodes[i]->appendNewEdge(this->nodes[j], newValue);
            }
        }
    }
}

void Graph::deepTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	  * Fill nodes array by travelling graph starting from first and using recursivity
	  */
    ///////// ca crash mais je sais pas du tout pourquoi //////////////
    int newValue = first->value;
    if(visited[newValue] == false)
    {
        visited[newValue] == true;
        Edge* newEdge = first->edges;
        while(newEdge != nullptr)
        {
            this->deepTravel(newEdge->destination, nodes, nodesSize, visited);
            newEdge = newEdge->next;
        }
    }
    

}

void Graph::wideTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	 * Fill nodes array by travelling graph starting from first and using queue
	 * nodeQueue.push(a_node)
	 * nodeQueue.front() -> first node of the queue
	 * nodeQueue.pop() -> remove first node of the queue
	 * nodeQueue.size() -> size of the queue
	 */
	std::queue<GraphNode*> nodeQueue;
	nodeQueue.push(first);
    while(nodeQueue.size() != 0)
    {
        visited[nodeQueue.front()->value] = true;
        nodeQueue.pop();
    }
}

bool Graph::detectCycle(GraphNode *first, bool visited[])
{
	/**
	  Detect if there is cycle when starting from first
	  (the first may not be in the cycle)
	  Think about what's happen when you get an already visited node
	**/
    visited[first->value] = true;
    Edge* newEdge;
    for(newEdge = first->edges; newEdge != nullptr; newEdge = newEdge->next)
    {
        if(!visited[newEdge->destination->value])
        {
            if(detectCycle(newEdge->destination, visited))
            {
                return true;
            }
        }
        else
        {
            return true;
        }
    }
    return false;
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 150;
	w = new GraphWindow();
	w->show();

	return a.exec();
}
